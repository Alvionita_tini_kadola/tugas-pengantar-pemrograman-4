import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
	
	
	
public class KombinasiPermutasi{
	static Scanner input=new Scanner(System.in);
	public static void main(String args[]) throws IOException {


		MainMenu();
	}
	static void MainMenu() throws IOException{
		int pilihan =-1;
		do{
			System.out.println("Main Menu");
			System.out.println("1. Permutasi");
			System.out.println("2. Kombinasi");
			System.out.println("0. Exit");
			do{
				try{
					System.out.print("Masukkan pilihan : ");
					pilihan=input.nextInt();
					break;
					}catch(InputMismatchException e){
						System.out.println("Pilihan tidak ada, masukkan lagi");
						input.nextLine();
					}
			}while(true);
			switch(pilihan){
			case 1:
				permutasi();
				break;
			case 2:
				kombinasi();
				break;
			case 0:
				break;
			}
		}while(pilihan!=0);
	}	
	static void permutasi() throws IOException{
		FileInputStream in = null;
		try{
			in = new FileInputStream("variabel.txt");
			System.out.println("-------------------------");
			System.out.println("Permutasi");
			int N=0,R=0,NR;
			
			String string="";
			int reader;
			boolean negative = false;
			do{
				reader=(char)in.read();
				if (reader==45)negative=true;		
				else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				if (reader ==' ')break;
				if(reader!=45)string=string+String.valueOf((char)reader);
			}while(true);
		
			N =Integer.valueOf(string);
			if(negative)N=-N;
			negative=false;
			System.out.println("Nilai N : "+N);
			string="";
			
			
			if ((reader=in.read())==(-1))System.out.println("akhir dari file");
			
			while(true){
				
				
				if (reader==45)negative=true;		
				else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				if(reader!=45)string=string+String.valueOf((char)reader);
				if ((reader=in.read())==-1)break;
				else continue;
				
			}
			
			R =Integer.valueOf(string);
			if(negative)R=-R;
			negative=false;
			System.out.println("Nilai R : "+R);
			if(N<R){
				throw new Exception();
			}
			int Ntotal=1;
			int NRtotal=1;
			
			
			NR=N-R;
			for(int index =1;index<=N;index++){
				Ntotal*=index;
			}
			for(int index =1;index<=NR;index++){
				NRtotal*=index;
			}
			System.out.println("Permutasi nilai dari N dan R : "+(Ntotal/NRtotal));
			System.out.println("-------------------------");
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini membutuhkan variabel.txt (dengan 2 variabel positif di dalamnya) untuk dijalankan");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println(" variabel.txt tidak mengandung 2 nilai (pisahkan dengan spasi dan nilai harus positif)");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println(" variabel.txt tidak mengandung 2 nilai  (pisahkan dengan spasi dan nilai harus positif)");
			System.out.println("-------------------------");
		}catch(Exception e){
			System.out.println("Nilai N lebih kecil daripada nilai R");
			System.out.println("Ubah nilai pada variabel.txt (Nilai N harus lebih besar daripada nilai R)");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
				}
		}
	}
	static void kombinasi() throws IOException{
		FileInputStream in = null;
		try{
			in = new FileInputStream("variabel.txt");
			System.out.println("-------------------------");
		System.out.println("Kombinasi");
		int N,R,NR;
		String string="";
		int reader;
		boolean negative = false;
		do{
			reader=(char)in.read();
			if (reader==45)negative=true;		
			else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
				throw new IOException();
			}
			if (reader ==' ')break;
			if(reader!=45)string=string+String.valueOf((char)reader);
		}while(true);
	
		N =Integer.valueOf(string);
		if(negative)N=-N;
		negative=false;
		System.out.println("Nilai N : "+N);
		string="";
		
		
		if ((reader=in.read())==(-1))System.out.println("akhir dari file");
		
		while(true){
			
			
			if (reader==45)negative=true;		
			else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
				throw new IOException();
			}
			if(reader!=45)string=string+String.valueOf((char)reader);
			if ((reader=in.read())==-1)break;
			else continue;
			
		}
		
		R =Integer.valueOf(string);
		if(negative)R=-R;
		negative=false;
		System.out.println("Nilai R : "+R);
		if(N<R){
			throw new Exception();
		}
		int Ntotal=1;
		int Rtotal=1;
		int NRtotal=1;
		
		NR=N-R;
		for(int index =1;index<=N;index++){
			Ntotal*=index;
		}
		for(int index =1;index<=R;index++){
			Rtotal*=index;
		}
		for(int index =1;index<=NR;index++){
			NRtotal*=index;
		}
		System.out.println("Kombinasi dari N dan R : "+(Ntotal/(Rtotal*NRtotal)));
		System.out.println("-------------------------");
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini membutuhkan variabel.txt (dengan 2 variabel positif di dalamnya) untuk dijalankan");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println("variabel.txt tidak mengandung 2 nilai (pisahkan dengan spasi dan masukkan nilai positif)");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println("variabel.txt tidak mengandung 2 nilai (pisahkan dengan spasi dan masukkan nilai positif)");
			System.out.println("-------------------------");
		}catch(Exception e){
			System.out.println("Nilai N lebih kecil daripada nilai R");
			System.out.println("Ubah nilai pada variabel.txt (Nilai N harus lebih besar daripada nilai R)");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
				}
		}
	}
}