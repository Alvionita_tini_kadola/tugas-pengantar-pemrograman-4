import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class CopyPaste{
	static Scanner input=new Scanner(System.in);
	public static void main(String args[]) throws IOException {
		
	
				FileInputStream in = null;
				FileOutputStream out = null;
				try {
					
					System.out.println("Masukkan lokasi file yang akan dipindahkan ");
					System.out.println("Contoh: ('C:/java/text/')");
					System.out.println("Jika file berada di folder yang sama dengan program ini, ketik 'default'");
					String inSource=input.next();
					String outSource;
					if (inSource.contentEquals("default")){
						inSource= "";
					}
					
					System.out.println("Masukkan file yang akan dipindahkan (.txt)");
					System.out.println("Contoh: 'teks.txt'");
					String filename=input.next();
					
					do{
						System.out.println("Masukkan tujuan lokasi file yang akan disalin");
						System.out.println("Contoh: ('E:/javaku/')");
						outSource=input.next();
						
						if(outSource.contentEquals(inSource)){
							System.out.println("Masukkan lokasi tujuan file yang lain");
							continue;
						}
						break;
					}while(true);
					
				
					
					in = new FileInputStream(inSource+filename);
					out = new FileOutputStream(outSource+filename);
					int copy;
					
					while ((copy = in.read()) != -1) {		//-1 means end of file
						out.write(copy);
					}
					System.out.println(filename+" berhasil disalin ke"+outSource);
					}catch(FileNotFoundException e){
						System.out.println("File tidak ditemukan");
					}finally {
						if (in != null) {
						in.close();
						}
						if (out != null) {
						out.close();
						}
					}
	
		input.nextLine();
	}	
	}