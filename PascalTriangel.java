import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
	
	
	
public class PascalTriangel{
	static Scanner input=new Scanner(System.in);
	public static void main(String args[]) throws IOException {
		FileInputStream in = null;
		FileOutputStream out = null;
		
		try{
			in = new FileInputStream("pascalInput.txt");
			out = new FileOutputStream("pascalOutput.txt");
			int rows;
			int reader;
			String string="";
			while((reader=in.read()) !=-1){
				if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				string=string+String.valueOf((char)reader);
				
				if((reader=in.read()) !=-1){
					
					string=string+String.valueOf((char)reader);
				}
				
			}
			rows =Integer.valueOf(string);
			
			String newLine = System.getProperty("line.separator");
			
			int indeks_row,indeks_number;			// indeks_row represents the current row to print(starting from index 0)
										// indeks_number represents the current sequence of number to print(starting from 1)
      
	        for (indeks_row = 0; indeks_row < rows; indeks_row++) {				//number of rows to print
	        	
		            int spaces =(rows-indeks_row);					//number of spaces to print
		          
		            for (int x=0; x<spaces;x++){//printing spaces before the first number on each row
		            	string="   ";
		            	for(int index=0;index<string.length();index++){
		    	        	out.write(string.charAt(index));
		    	        }	
		    		}		              
		            long number=1;								//starting from number 1		            
		            for (indeks_number = 0; indeks_number <= indeks_row; indeks_number++){	                
		                if(number<10){
		                	string="  "+number+"   ";
		                	for(int index=0;index<string.length();index++){
			    	        	out.write(string.charAt(index));
			    	        }
		                		 	 //printing 5 spaces for 1 digit number
		                }else if(number<100){
		                	string=("  "+number+"  "); 		 //printing 4 spaces for 2 digits number
		                	for(int index=0;index<string.length();index++){
			    	        	out.write(string.charAt(index));
			    	        }
		                }else if(number<1000){
		                	string=(" "+number+"  ");  		 //printing 3 spaces for 3 digits number
		                	for(int index=0;index<string.length();index++){
			    	        	out.write(string.charAt(index));
			    	        }
		                }else{
		                	string=(" "+number+" ");  		 	 //printing 2 spaces for 4 digits or more number
		                	for(int index=0;index<string.length();index++){
			    	        	out.write(string.charAt(index));
			    	        }
		                }
		                number=number*(indeks_row - indeks_number) / (indeks_number + 1);			 //the pascal'string pattern
		            }
		            string=newLine;
		            for(int index=0;index<string.length();index++){
	    	        	out.write(string.charAt(index));
	    	        }
	       }
	        System.out.println("pascalOutput.txt berhasil dibuat");
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini membutuhkan pascalInput.txt (dengan nilai jumlah baris) untuk di compile");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println("pascalInput.txt tidak mengandung nilai");
			System.out.println("Ubah nilai pascalInput.txt");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println("pascalInput.txt tidak mengandung nilai");
			System.out.println("Ubah nilai pada file pascalInput.txt");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
}
}
